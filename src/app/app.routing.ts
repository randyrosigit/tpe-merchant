import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgReduxModule} from '@angular-redux/store';
import {NgRedux, DevToolsExtension} from '@angular-redux/store';
import {rootReducer, ArchitectUIState} from './ThemeOptions/store';
import {ConfigActions} from './ThemeOptions/store/config.actions';
import {LoadingBarRouterModule} from '@ngx-loading-bar/router';

import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {AppComponent} from './app.component';

import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './_guards';

import {BaseLayoutComponent} from './Layout/base-layout/base-layout.component';
import {PagesLayoutComponent} from './Layout/pages-layout/pages-layout.component';

import { HomeComponent } from './Pages/Home/home.component';
import { LoginComponent } from './Pages/Login/login.component';
import { ForgotComponent } from './Pages/Login/forgot.component';

// Quests
import { QuestList } from './Pages/Quest/quest.list';
import { QuestAdd } from './Pages/Quest/quest.add';
import { QuestEdit } from './Pages/Quest/quest.edit';
import { QuestView } from './Pages/Quest/quest.view';
import { QuestcompleteRequests } from './Pages/Quest/questcomplete.requests';

// VOUCHER
import { VoucherList } from './Pages/Voucher/voucher.list';
import { VoucherAdd } from './Pages/Voucher/voucher.add';
import { VoucherEdit } from './Pages/Voucher/voucher.edit';
import { VoucherView } from './Pages/Voucher/voucher.view';
import { VoucherredeemRequests } from './Pages/Voucher/voucherredeem.requests';

// USER
import { UserList } from './Pages/User/user.list';
import { UserAdd } from './Pages/User/user.add';
import { UserEdit } from './Pages/User/user.edit';
import { UserView } from './Pages/User/user.view';
import { UserProfile } from './Pages/User/user.profile';

const appRoutes: Routes = [
	{
	    path: '',
	    component: BaseLayoutComponent,
	    canActivate: [AuthGuard],
	    children: [
	      {path: '', component: HomeComponent, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'quest', component: QuestList, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'quest/add', component: QuestAdd, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'quest/edit/:id', component: QuestEdit, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'quest/view/:id', component: QuestView, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'complete_requests', component: QuestcompleteRequests, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'voucher', component: VoucherList, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'voucher/add', component: VoucherAdd, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'voucher/edit/:id', component: VoucherEdit, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'voucher/view/:id', component: VoucherView, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'redeem_requests', component: VoucherredeemRequests, data: {extraParameter: 'dashboardsMenu'}}, 
	      {path: 'profile', component: UserProfile, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'user', component: UserList, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'user/add', component: UserAdd, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'user/edit/:id', component: UserEdit, data: {extraParameter: 'dashboardsMenu'}},
	      {path: 'user/view/:id', component: UserView, data: {extraParameter: 'dashboardsMenu'}},
	    ]
	},
	{
    path: '',
    component: PagesLayoutComponent,
    children: [
      {path: 'login', component: LoginComponent, data: {extraParameter: ''}},
      {path: 'forgot-password', component: ForgotComponent, data: {extraParameter: ''}}
    ]
  },
]



export const routing = RouterModule.forRoot(appRoutes);