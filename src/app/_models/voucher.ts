export class Voucher {
    voucher_id: number;
    voucher_name: string;
    voucher_expiry_date: string;
    voucher_created_on: string;
    status: string;
    message: string;
    result_set: Array<any>;
}