export class Inventory {
    inventory_id: number;
    voucher_name: string;
    customer_name: string;    
    inventory_created_on: string;
    inventory_status: string;
    status: string;
    message: string;
    result_set: Array<any>;
}