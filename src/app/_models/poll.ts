export class Poll {
    poll_id: number;
    poll_name: string;
    poll_merchant_id: string;
    poll_created_on: string;
    status: string;
    message: string;
    result_set: Array<any>;
}