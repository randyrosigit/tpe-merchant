export class Merchant {
    merchant_id: number;
    merchant_name: string;
    merchant_status: string;
    merchant_created_on: string;
    status: string;
    message: string;
    result_set: Array<any>;
}
