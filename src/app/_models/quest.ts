export class Quest {
    quest_id: number;
    quest_name: string;
    quest_expiry_date: Date;
    quest_created_on: string;
    status: string;
    message: string;
    result_set: Array<any>;
}