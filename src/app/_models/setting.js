export class Setting {
    site_name: string;
    site_email: string;
    site_phoneno: string;
    site_logo: string;
    site_currency_symbol: string;
    status: string;
    message: string;
    result_set: Array<any>;
}