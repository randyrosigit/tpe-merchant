export class Category {
    category_id: number;
    category_name: string;
    category_status: string;
    category_created_on: string;
    status: string;
    message: string;
    result_set: Array<any>;
}
