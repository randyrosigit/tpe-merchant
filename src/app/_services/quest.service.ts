import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

// import { Quest } from '../_models';

/*const HttpUploadOptions = {
  headers: new Headers()
}*/

@Injectable({ providedIn: 'root' })
export class QuestService {
    constructor(public http: HttpClient) { }

    public getAll() {
        return this.http.get<any>(`quest/list`);
    }

    getById(id: number) {
        return this.http.get(`quest/edit/` + id);
    }

    insert(quest: any) {
        return this.http.post(`quest/add`, quest);
    }

    update(id:number, quest: any) {
        return this.http.post(`quest/update/` + id, quest);
    }

    delete(id: number) {
        return this.http.get(`quest/delete/` + id);
    }

    featured(id: number) {
        return this.http.get(`quest/featured/` + id);
    }

    public getAllCompleteRequests(filter = {params:{}}) {
        return this.http.get<any>(`quest/complete_requests`, filter);
    }

    acceptQuest(quest: any) {
        return this.http.post(`quest/accept_request`, quest);
    }

    deleteRequest(quest: any) {
        return this.http.post(`quest/delete_request`, quest);
    }
}
