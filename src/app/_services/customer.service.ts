import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// import { Customer } from '../_models';

@Injectable({ providedIn: 'root' })
export class CustomerService {
    constructor(public http: HttpClient) { }

    public getAll(filter = {params:{}}) {
        return this.http.get<any>(`customer/list`, filter);
    }

    getById(id: number) {
        return this.http.get<any>(`customer/edit/` + id);
    }

    insert(customer: any) {
        return this.http.post(`customer/insert`, customer);
    }

    update(id:number, customer: any) {
        return this.http.post(`customer/update/` + id, customer);
    }

    delete(id: number) {
        return this.http.get(`customer/delete/` + id);
    }
}
