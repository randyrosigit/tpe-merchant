import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// import { Voucher } from '../_models';

@Injectable({ providedIn: 'root' })
export class VoucherService {
    constructor(public http: HttpClient) { }

    public getAll(filter = {params:{}}) {
        return this.http.get<any>(`voucher/list`, filter);
    }

    getById(id: number) {
        return this.http.get(`voucher/edit/` + id);
    }

    insert(voucher: any) {
        return this.http.post(`voucher/add`, voucher);
    }

    update(id:number, voucher: any) {
        return this.http.post(`voucher/update/` + id, voucher);
    }

    delete(id: number) {
        return this.http.get(`voucher/delete/` + id);
    }

    featured(id: number) {
        return this.http.get(`voucher/featured/` + id);
    }

    approved(id: number) {
        return this.http.get(`voucher/approved/` + id);
    }

    getAllRedeemRequests(filter = {params:{}}) {
        return this.http.get<any>(`voucher/redeem_requests`, filter);
    }

    CompleteRedeem(data: any) {
        return this.http.post(`voucher/redeem_complete/`, data);
    }

    VoucherExchange(data: any) {
        return this.http.post(`voucher/voucher_exchange/`, data);
    }

    DeleteRequest(data: any) {
        return this.http.post(`voucher/delete_request/`, data);
    }
}
