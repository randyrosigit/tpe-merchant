import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class OrderService {
    constructor(public http: HttpClient) { }

    public getAll(filter = {params:{}}) {
        return this.http.get<any>(`order/list`, filter);
    }

    getById(id: number) {
        return this.http.get(`order/info/` + id);
    }

    /*insert(quest: Quest) {
        return this.http.post(`quest/add`, quest);
    }

    update(id:number, quest: Quest) {
        return this.http.post(`quest/update/` + id, quest);
    }

    delete(id: number) {
        return this.http.post(`quest/delete/` + id);
    }*/
}
