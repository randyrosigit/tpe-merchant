import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class RoleService {
    constructor(public http: HttpClient) { }

    public getAll(filter = {params:{}}) {
        return this.http.get<any>(`role/list`, filter);
    }

    getById(id: number) {
        return this.http.get(`role/edit/` + id);
    }

    insert(role: any) {
        return this.http.post(`role/add`, role);
    }

    update(id:number, role: any) {
        return this.http.post(`role/update/` + id, role);
    }

    delete(id: number) {
        return this.http.get(`role/delete/` + id);
    }
}
