import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// import { Poll } from '../_models';

@Injectable({ providedIn: 'root' })
export class PollService {
    constructor(public http: HttpClient) { }

    public getAll(filter = {params:{}}) {
        return this.http.get<any>(`poll/list`, filter);
    }

    getById(id: number) {
        return this.http.get(`poll/edit/` + id);
    }

    insert(poll: any) {
        return this.http.post(`poll/insert`, poll);
    }

    update(id:number, poll: any) {
        return this.http.post(`poll/update/` + id, poll);
    }

    delete(id: number) {
        return this.http.get(`poll/delete/` + id);
    }

    featured(id: number) {
        return this.http.get(`poll/featured/` + id);
    }
}
