import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class TemplateService {
    constructor(public http: HttpClient) { }

    public getAll(filter = {params:{}}) {
        return this.http.get<any>(`emailtemplate/list`, filter);
    }

    getById(id: number) {
        return this.http.get(`emailtemplate/edit/` + id);
    }

    insert(template: any) {
        return this.http.post(`emailtemplate/add`, template);
    }

    update(id:number, template: any) {
        return this.http.post(`emailtemplate/update/` + id, template);
    }

    delete(id: number) {
        return this.http.get(`emailtemplate/delete/` + id);
    }
}
