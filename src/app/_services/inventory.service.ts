import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

// import { Inventory } from '../_models';

/*const HttpUploadOptions = {
  headers: new Headers()
}*/

@Injectable({ providedIn: 'root' })
export class InventoryService {
    constructor(public http: HttpClient) { }

    public getAll(filter = {params:{}}) {
        return this.http.get<any>(`inventory/list`, filter);
    }

    /*getById(id: number) {
        return this.http.get(`quest/edit/` + id);
    }

    insert(quest: Quest) {
        return this.http.post(`quest/add`, quest);
    }

    update(id:number, quest: Quest) {
        return this.http.post(`quest/update/` + id, quest);
    }

    delete(id: number) {
        return this.http.post(`quest/delete/` + id);
    }*/
}
