import {Component, HostListener, OnInit} from '@angular/core';
import {ThemeOptions} from '../../../theme-options';
import {select} from '@angular-redux/store';
import {Observable} from 'rxjs';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
})
export class SidebarComponent implements OnInit {
  public extraParameter: any;

  constructor(public globals: ThemeOptions, private activatedRoute: ActivatedRoute) {

  }

  @select('config') public config$: Observable<any>;

  private newInnerWidth: number;
  private innerWidth: number;
  activeId = 'dashboardsMenu';
  disactive: any = {'users': false, 'voucher': false, 'redeem_requests': false, 'quest': false, 'complete_requests': false};

  toggleSidebar() {
    this.globals.toggleSidebar = !this.globals.toggleSidebar;
  }

  sidebarHover() {
    this.globals.sidebarHover = !this.globals.sidebarHover;
  }

  ngOnInit() {
    setTimeout(() => {
      this.innerWidth = window.innerWidth;
      if (this.innerWidth < 1200) {
        this.globals.toggleSidebar = true;
      }
    });

    this.extraParameter = this.activatedRoute.snapshot.firstChild.data.extraParameter;

    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if(currentUser) {
      let permissionss = String(currentUser.permissions);

      if(permissionss == 'all') {
        this.disactive = {'user': true, 'voucher': true, 'redeem_requests': true, 'quest': true, 'complete_requests': true};
      } else {
        for(let permission of permissionss.split(',')){
          if(permission == '/user/') {
            this.disactive.user = true;
          } else if(permission == '/voucher/') {
            this.disactive.voucher = true;
          } else if(permission == '/redeem_requests/') {
            this.disactive.redeem_requests = true;
          } else if(permission == '/quest/') {
            this.disactive.quest = true;
          } else if(permission == '/complete_requests/') {
            this.disactive.complete_requests = true;
          } 
        }
      }
      
    }

  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.newInnerWidth = event.target.innerWidth;

    if (this.newInnerWidth < 1200) {
      this.globals.toggleSidebar = true;
    } else {
      this.globals.toggleSidebar = false;
    }

  }
}
