import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { AuthenticationService } from '../_services';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthenticationService) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with jwt token if available
        let currentUser = this.authenticationService.currentUserValue;
        if (currentUser && currentUser['token']) {
            request = request.clone({
                headers: new HttpHeaders({
                  'Auth': `Bearer:${currentUser['token']}`
                })
            });
        }

        request = request.clone({ 
            //url: `http://teamworks/team1/picky/merchantapi/${request.url}`
           url:  `https://api.thepicky.co/index.php/merchantapi/${request.url}`
        });

        return next.handle(request);
    }
}