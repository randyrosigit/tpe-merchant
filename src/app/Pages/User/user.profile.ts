import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { MustMatch } from '../../_helpers/must-match.validator';
import { AlertService, UserService } from '../../_services';

@Component({templateUrl: 'user.profile.html'})
export class UserProfile implements OnInit {
    heading = 'My Account';
    subheading = '';
    icon = 'fa fa-user icon-gradient bg-happy-itmeo';
    userForm: FormGroup;
    loading = false;
    submitted = false;
    admin_profile = null;
    preview_admin_profile = null;

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private route: ActivatedRoute,
        private alertService: AlertService,
        private userService: UserService        
    ) {  
    }

    ngOnInit() {
        this.getUser();
        this.userForm = this.formBuilder.group({
            merchant_name: ['', Validators.required],
            merchant_email: ['', [Validators.required, Validators.email]],
            merchant_password: [''],
            merchant_confirm_password: [''],
            merchant_logo: ['', Validators.required]
        }, {
            validator: MustMatch('merchant_password', 'merchant_confirm_password')
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.userForm.controls; }

    getUser() {
        this.userService.getProfile().subscribe((data: any) => {
            var user = data.result_set;
            this.userForm.setValue({
              merchant_name: user.merchant_name,
              merchant_email: user.merchant_email,
              merchant_password: '',
              merchant_confirm_password: '',
              merchant_logo: user.merchant_logo
            });
            this.preview_admin_profile = user.merchant_logo;
        });
    }

    IconChange(files: FileList) {
        this.admin_profile = files[0];
        this.userForm.get('merchant_logo').setValue(this.admin_profile.name);
        var reader = new FileReader();
        reader.readAsDataURL(this.admin_profile); 
        reader.onload = (_event) => { 
          this.preview_admin_profile = reader.result; 
        }
    }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.userForm.invalid) {
            return;
        }

        this.loading = true;

        const formData = new FormData();
        formData.append('merchant_name', this.userForm.get('merchant_name').value);
        formData.append('merchant_email', this.userForm.get('merchant_email').value);
        formData.append('merchant_password', this.userForm.get('merchant_password').value);

        if(this.admin_profile!=null) {
          formData.append('merchant_logo', this.admin_profile, this.admin_profile.name);
        }

        this.userService.profileupdate(formData).pipe(first()).subscribe((data: any) => {
              if(data.status=='ok') {
                this.userForm.reset();
                this.getUser();
                this.alertService.success(data.message, true);
                this.router.navigate(['/']);
              } else {
                this.alertService.error(data.message, true);
              }
            },
            error => {
                this.alertService.error(error);
                this.loading = false;
            }
        );
    }
}