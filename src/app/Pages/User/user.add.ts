import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';
import { first } from 'rxjs/operators';

import { MustMatch } from '../../_helpers/must-match.validator';
import { AlertService, UserService } from '../../_services';
import { Utils } from '../../_helpers';

@Component({ templateUrl: 'user.add.html'})
export class UserAdd implements OnInit {
    heading = 'Add User';
    subheading = '';
    icon = 'fa fa-user icon-gradient bg-happy-itmeo';
    userForm: FormGroup;
    loading = false;
    submitted = false;
    admin_profile = null;
    preview_admin_profile = null;

    users: any;
    vouchers: any;
    redeem_requests: any;
    quests: any;
    complete_requests: any;

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private alertService: AlertService,
        private userService: UserService
    ) { }

    ngOnInit() {
        this.userForm = this.formBuilder.group({
            name: ['', [Validators.required, Utils.noWhitespaceValidator]],
            email: ['', [Validators.required, Utils.noWhitespaceValidator, Validators.pattern('[a-zA-Z0-9.-]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}')]],
            password: ['', [Validators.required, Utils.noWhitespaceValidator]],
            confirm_password: ['', [Validators.required, Utils.noWhitespaceValidator]],
            admin_profile: [''],
            status: ['1', [Validators.required, Utils.noWhitespaceValidator]],
            roles: this.formBuilder.array([])
        }, {
            validator: MustMatch('password', 'confirm_password')
        });

        this.users = [{'title': 'User Add', 'value': '/user/add', 'checked': false, 'disabled': false},{'title': 'User Edit', 'value': '/user/edit', 'checked': false, 'disabled': false},{'title': 'User Delete', 'value': '/user/delete', 'checked': false, 'disabled': false}];
        this.vouchers = [{'title': 'Voucher Add', 'value': '/voucher/add', 'checked': false, 'disabled': false},{'title': 'Voucher Edit', 'value': '/voucher/edit', 'checked': false, 'disabled': false},{'title': 'Voucher Delete', 'value': '/voucher/delete', 'checked': false, 'disabled': false}];
        this.redeem_requests = [{'title': 'Voucher Redeem Request', 'value': '/redeem_requests/', 'checked': false, 'disabled': false}];
        this.quests = [{'title': 'Quest Add', 'value': '/quest/add', 'checked': false, 'disabled': false},{'title': 'Quest Edit', 'value': '/quest/edit', 'checked': false, 'disabled': false},{'title': 'Quest Delete', 'value': '/quest/delete', 'checked': false, 'disabled': false}];
        this.complete_requests = [{'title': 'Quest Complete Requests', 'value': '/complete_requests/', 'checked': false, 'disabled': false}];
    }

    IconChange(files: FileList) {
        this.admin_profile = files[0];
        this.userForm.get('admin_profile').setValue(this.admin_profile.name);
        var reader = new FileReader();
        reader.readAsDataURL(this.admin_profile); 
        reader.onload = (_event) => { 
          this.preview_admin_profile = reader.result; 
        }
    }

    onChange(event) {
      const roles = <FormArray>this.userForm.get('roles') as FormArray;

      if(event.checked) {
        roles.push(new FormControl(event.source.value))
      } else {
        const i = roles.controls.findIndex(x => x.value === event.source.value);
        roles.removeAt(i);
      }
    }

    // convenience getter for easy access to form fields
    get f() { return this.userForm.controls; }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.userForm.invalid) {
            return;
        }

        this.loading = true;

        const formData = new FormData();
        formData.append('merchant_user_name', this.userForm.get('name').value);
        formData.append('merchant_user_email', this.userForm.get('email').value);
        formData.append('merchant_user_password', this.userForm.get('password').value);
        formData.append('merchant_user_status', this.userForm.get('status').value);
        formData.append('rules', JSON.stringify(this.userForm.get('roles').value));

        if(this.admin_profile!=null) {
          formData.append('merchant_user_logo', this.admin_profile, this.admin_profile.name);
        }

        this.userService.insert(formData).pipe(first()).subscribe((data: any) => {
            if(data.status=='ok') {
              this.alertService.success(data.message, true);
              this.router.navigate(['/user']);
            } else {
              this.alertService.error(data.message, true);
            }
          },
          error => {
              this.alertService.error(error);
              this.loading = false;
          }
        );
    }
}