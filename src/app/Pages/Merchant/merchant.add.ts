import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import {AngularEditorConfig} from '@kolkov/angular-editor';

import { AlertService, MerchantService, CategoryService } from '../../_services';
import { Utils } from '../../_helpers';

@Component({templateUrl: 'merchant.add.html'})
export class MerchantAdd implements OnInit {
    heading = 'Add Merchant';
    subheading = '';
    icon = 'fa fa-simplybuilt icon-gradient bg-happy-itmeo';
    merchantForm: FormGroup;
    loading = false;
    submitted = false;
    categorys = [];
    merchant_logo = null;
    preview_merchant_logo = null;

    wyswigconfig: AngularEditorConfig = {
      editable: true,
      spellcheck: true,
      height: '15rem',
      minHeight: '5rem',
      placeholder: 'Enter text here...',
      translate: 'no',
      customClasses: [
        {
          name: 'quote',
          class: 'quote',
        },
        {
          name: 'redText',
          class: 'redText'
        },
        {
          name: 'titleText',
          class: 'titleText',
          tag: 'h1',
        },
      ]
    };

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private categoryService: CategoryService,
        private merchantService: MerchantService,
        private alertService: AlertService
    ) { }

    ngOnInit() {
        this.merchantForm = this.formBuilder.group({
            merchant_name: ['', [Validators.required, Utils.noWhitespaceValidator]],
            merchant_category_id: ['', [Validators.required, Utils.noWhitespaceValidator]],
            merchant_description: ['', Validators.required],
            merchant_logo: ['', [Validators.required, Utils.noWhitespaceValidator]],
            merchant_status: ['', [Validators.required, Utils.noWhitespaceValidator]]
        });
        this.loadAllCategorys();
    }

    private loadAllCategorys() {
        this.categoryService.getAll({ params: {filter_category_status:1} }).pipe(first()).subscribe((categorys: any) => {
            if(categorys.status=='ok') {
                this.categorys = categorys.result_set;
            }
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.merchantForm.controls; }

    IconChange(files: FileList) {
        this.merchant_logo = files[0];
        this.merchantForm.get('merchant_logo').setValue(this.merchant_logo.name);
        var reader = new FileReader();
        reader.readAsDataURL(this.merchant_logo); 
        reader.onload = (_event) => { 
          this.preview_merchant_logo = reader.result; 
        }
    }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.merchantForm.invalid) {
            return;
        }

        this.loading = true;

        const formData = new FormData();
        formData.append('merchant_name', this.merchantForm.get('merchant_name').value);
        formData.append('merchant_category_id', this.merchantForm.get('merchant_category_id').value);
        formData.append('merchant_description', this.merchantForm.get('merchant_description').value);
        formData.append('merchant_logo', this.merchant_logo, this.merchant_logo.name);
        formData.append('merchant_status', this.merchantForm.get('merchant_status').value);

        this.merchantService.insert(formData).pipe(first()).subscribe((data: any) => {
              if(data.status=='ok') {
                this.alertService.success(data.message, true);
                this.router.navigate(['/merchant']);
              } else {
                this.alertService.error(data.message, true);
              }
            },
            error => {
                this.alertService.error(error);
                this.loading = false;
            }
        );
    }
}