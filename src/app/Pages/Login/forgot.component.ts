import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';

import { AlertService, AuthenticationService,UserService } from '../../_services';
import { Utils } from '../../_helpers';

@Component({
  selector: 'app-login-boxed',
  templateUrl: './forgot.component.html',
  styles: []
})
export class ForgotComponent implements OnInit {
	forgotForm: FormGroup;
	returnUrl: string;
	submitted = false;

  constructor(
  	private formBuilder: FormBuilder,
  	private route: ActivatedRoute,
  	private router: Router,
  	private authenticationService: AuthenticationService,
  	private alertService: AlertService,
    private userService: UserService
  ) {
		if (this.authenticationService.currentUserValue) { 
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {
  	this.forgotForm = this.formBuilder.group({
      username: ['', [Validators.required, Utils.noWhitespaceValidator, Validators.pattern('[a-zA-Z0-9.-]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}')]],
    });

    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  get f() { return this.forgotForm.controls; }

  onSubmit(){

  	this.submitted = true;
  	
    if (this.forgotForm.invalid) {
        return;
    }

    const formData = new FormData();
    formData.append('username', this.f.username.value);

    this.userService.forgotpassword(formData).pipe(first()).subscribe((data: any) => {
      if(data.status=='ok') {
        this.alertService.success(data.message, true);
        this.router.navigate(['/login']);
      } else {
        this.alertService.error(data.message, true);
      }
    },error => {
        this.alertService.error(error);
    });
  }

}
