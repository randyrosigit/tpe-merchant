import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
import {MatTableDataSource, MatSort, MatPaginator} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';

// import { Quest } from '../../_models';
import { AlertService, QuestService } from '../../_services';

@Component({ templateUrl: 'quest.list.html' })
export class QuestList implements OnInit, OnDestroy {
    
    heading = 'Quests';
	  subheading = '';
	  icon = 'fa fa-cubes icon-gradient bg-happy-itmeo';
	  link = '/quest/add';
    quests: [];
    dataSource: MatTableDataSource<any[]>;
    search_key = "";
    pageSizeOptions = null;

    displayedColumns = [ 's_no','quest_name', 'voucher_name', 'quest_limit_option', 'quest_limit', 'quest_admin_approved', 'quest_created_on',  'actions'];
    

    @ViewChild(MatPaginator) paginator: MatPaginator;  
    @ViewChild(MatSort) sort: MatSort;
    
    setPageSizeOptions(setPageSizeOptionsInput: string) {
      this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
    }

    constructor( 
        private questServices: QuestService,
        private alertService: AlertService 
    ) { }

    ngOnInit() {
        this.loadAllQuests();
    }

    ngOnDestroy() {
    }

    clearFilters(){
       this.dataSource.filter = '';
       this.search_key = '';
    }

    private loadAllQuests() {
        this.questServices.getAll().pipe(first()).subscribe((quests: any) => {
        	if(quests.status=='ok') {
                this.dataSource = new MatTableDataSource(quests.result_set);
                this.dataSource.paginator = this.paginator;  
                this.dataSource.sort = this.sort; 
        	}
        });
    }

    deleteQuest(id: number) {
        this.alertService.delete().then(data=>{
            if(data) {
                this.questServices.delete(id).pipe(first()).subscribe((data: any) => {
                    if(data.status=='ok') {
                      this.alertService.success(data.message, true);
                      this.loadAllQuests()
                    } else {
                      this.alertService.error(data.message, true);
                    }
                });
            } 
        });
    }

    featuredQuest(id: number) {
        this.questServices.featured(id).pipe(first()).subscribe((data: any) => {
            if(data.status=='ok') {
              this.alertService.success(data.message, true);
              this.loadAllQuests()
            } else {
              this.alertService.error(data.message, true);
            }
        });
    }

    applyFilter(filterValue: string) {  
      this.dataSource.filter = filterValue.trim().toLowerCase();  
    
      if (this.dataSource.paginator) {  
        this.dataSource.paginator.firstPage();  
      }  
    }

}
