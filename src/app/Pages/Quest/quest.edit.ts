import { Component, OnInit, NgModule } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import {AngularEditorConfig} from '@kolkov/angular-editor';

import { AlertService, QuestService, VoucherService } from '../../_services';
import { Utils } from '../../_helpers';

@Component({templateUrl: 'quest.edit.html'})
export class QuestEdit implements OnInit {
    heading = 'Edit Quest';
    subheading = '';
    icon = 'fa fa-cubes icon-gradient bg-happy-itmeo';
    questForm: FormGroup;
    loading = false;
    submitted = false;
    quest_challenge_icon = null;
    quest_challenge_image = null;
    quest_detail_image = null;
    quest_detail_image1 = null;
    quest_detail_image2 = null;
    vouchers = [];
    minDate = (new Date()).toISOString();
    quest_id: number;
    preview_challenge_icon = null;
    preview_challenge_image = null;
    preview_detail_image = null;
    preview_detail_image1 = null;
    preview_detail_image2 = null;
    ExpminDate = null;
    ExpmaxDate = null;
    DateShow = null;
    currentDate = new Date();
    limitShow = true;

    wyswigconfig: AngularEditorConfig = {
      editable: true,
      spellcheck: true,
      height: '5rem',
      minHeight: '5rem',
      placeholder: 'Enter text here...',
      translate: 'no',
      customClasses: [
        {
          name: 'quote',
          class: 'quote',
        },
        {
          name: 'redText',
          class: 'redText'
        },
        {
          name: 'titleText',
          class: 'titleText',
          tag: 'h1',
        },
      ]
    };

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private route: ActivatedRoute,
        private questService: QuestService,
        private alertService: AlertService,
        private voucherServices: VoucherService
    ) {  
        this.quest_id = this.route.snapshot.params['id'];
    }

    ngOnInit() {
        this.questForm = this.formBuilder.group({
            quest_name: ['', [Validators.required, Utils.noWhitespaceValidator]],
            quest_short_description: ['', [Validators.required, Utils.noWhitespaceValidator, Validators.maxLength(200)]],
            // quest_description: ['', [Validators.required, Utils.noWhitespaceValidator]],
            quest_challenge_icon: ['', [Validators.required, Utils.noWhitespaceValidator]],
            quest_challenge_image: ['', [Validators.required, Utils.noWhitespaceValidator]],
            quest_detail_image: ['', [Validators.required, Utils.noWhitespaceValidator]],
            quest_detail_image1: ['', [Validators.required, Utils.noWhitespaceValidator]],
            quest_detail_image2: ['', [Validators.required, Utils.noWhitespaceValidator]],
            quest_voucher_id: ['', Validators.required],
            // quest_sort_order: [''],
            quest_limit_option: ['', Validators.required],
            quest_limit: ['', Validators.required],
            quest_expiry_option: ['', Validators.required],
            quest_expiry_start_date: [''],
            quest_expiry_date: [''],
            quest_expiry_days: [''],
            quest_status: ['', [Validators.required, Utils.noWhitespaceValidator]],
            quest_publish_date: ['', Validators.required],
            quest_detail: ['', [Validators.required, Utils.noWhitespaceValidator]],
            quest_detail1: ['', [Validators.required, Utils.noWhitespaceValidator]],
            quest_detail2: ['', [Validators.required, Utils.noWhitespaceValidator]],
        });
        this.getQuest(this.quest_id);
        this.loadAllVouchers();
        this.StatusChange();
        this.ExpOptionChange();
        this.ExpStrtDateChange();
        this.ExpEndDateChange();
        this.limitChange();
    }

    StatusChange() {
      this.questForm.get('quest_status').valueChanges.subscribe((Status: any) => {
        if(Status=='1') {
          this.currentDate = new Date();
          if(!this.questForm.get('quest_publish_date')) {
            this.questForm.get('quest_publish_date').setValue(this.minDate);
          }
        } else {
          if(!this.questForm.get('quest_publish_date')) {
            this.questForm.get('quest_publish_date').setValue('');
          }   
          this.currentDate = null;       
        }
      });
    }

    ExpStrtDateChange() {
      this.questForm.get('quest_expiry_start_date').valueChanges.subscribe((Date: any) => {
        if(Date) {
          this.ExpminDate = Date;
        }
      });
    }

    ExpEndDateChange() {
      this.questForm.get('quest_expiry_date').valueChanges.subscribe((Date: any) => {
        if(Date) {
          this.ExpmaxDate = Date;
        }
      });
    }

    ExpOptionChange() {
      this.questForm.get('quest_expiry_option').valueChanges.subscribe((option: any) => {
        if(option=='1') {
          this.questForm.get('quest_expiry_days').setValidators(null);
          this.questForm.get('quest_expiry_date').setValidators([Validators.required]);
          this.DateShow = true;
        } else {
          this.questForm.get('quest_expiry_date').setValidators(null);
          this.questForm.get('quest_expiry_days').setValidators([Validators.required, Validators.min(1)]);
          this.DateShow = false;
        }
        this.questForm.get('quest_expiry_days').updateValueAndValidity();
        this.questForm.get('quest_expiry_date').updateValueAndValidity();
      });
    }

    limitChange() {
      this.questForm.get('quest_limit_option').valueChanges.subscribe((option: any) => {
        if(option=='2') {
          this.questForm.get('quest_limit').setValidators(null);
          this.limitShow = false;
        } else {
          this.limitShow = true;
          this.questForm.get('quest_limit').setValidators([Validators.required, Validators.min(1)]);
        }
        this.questForm.get('quest_limit').updateValueAndValidity();
      });
    }

    // convenience getter for easy access to form fields
    get f() { return this.questForm.controls; }

    getQuest(id) {
        this.questService.getById(id).subscribe((data: any) => {
            var quest = data.result_set;

            var quest_expiry_start_date = null;
            var quest_expiry_date = null;
            var quest_expiry_days = null;

            if(quest.quest_expiry_option=='1') {
              if(quest.quest_expiry_start_date) {
                quest_expiry_start_date = new Date(quest.quest_expiry_start_date);
              }
              if(quest.quest_expiry_end_date) {
                quest_expiry_date = new Date(quest.quest_expiry_end_date);
              }
            } else {
              quest_expiry_days = quest.quest_expiry_days;
            }

            this.questForm.setValue({
              quest_name: quest.quest_name,
              quest_short_description: quest.quest_short_description,
              // quest_description: quest.quest_description,
              quest_challenge_icon: quest.quest_challenge_icon,
              quest_challenge_image: quest.quest_challenge_image,
              quest_detail_image: quest.quest_detail_image,
              quest_detail_image1: quest.quest_detail_image1,
              quest_detail_image2: quest.quest_detail_image2,
              quest_voucher_id: quest.quest_voucher_id,
              // quest_sort_order: quest.quest_sort_order,
              quest_limit_option: quest.quest_limit_option,
              quest_limit: quest.quest_limit,
              quest_expiry_option: quest.quest_expiry_option,
              quest_expiry_start_date: quest_expiry_start_date,
              quest_expiry_date: quest_expiry_date,
              quest_expiry_days: quest_expiry_days,
              quest_status: quest.quest_status,
              quest_publish_date: new Date(quest.quest_publish_date),
              quest_detail: quest.quest_detail,
              quest_detail1: quest.quest_detail1,
              quest_detail2: quest.quest_detail2,
            });
            this.preview_challenge_icon = quest.quest_challenge_icon;
            this.preview_challenge_image = quest.quest_challenge_image;
            this.preview_detail_image = quest.quest_detail_image;
            this.preview_detail_image1 = quest.quest_detail_image1;
            this.preview_detail_image2 = quest.quest_detail_image2;
        });
    }

    private loadAllVouchers() {
        this.voucherServices.getAll({ params: {filter_valid_voucher:'yes', filter_voucher_type: '1'} }).pipe(first()).subscribe((vouchers: any) => {
            if(vouchers.status=='ok') {
                this.vouchers = vouchers.result_set;
            }
        });
    }

    IconChange(files: FileList) {
        this.quest_challenge_icon = files[0];
        this.questForm.get('quest_challenge_icon').setValue(this.quest_challenge_icon.name);
        var reader = new FileReader();
        reader.readAsDataURL(this.quest_challenge_icon); 
        reader.onload = (_event) => { 
          this.preview_challenge_icon = reader.result; 
        }
    }

    ImageChange(files: FileList) {
        this.quest_challenge_image = files[0];
        this.questForm.get('quest_challenge_image').setValue(this.quest_challenge_image.name);
        var reader = new FileReader();
        reader.readAsDataURL(this.quest_challenge_image); 
        reader.onload = (_event) => { 
          this.preview_challenge_image = reader.result; 
        }
    }

    DtlImageChange(files: FileList) {
        this.quest_detail_image = files[0];
        this.questForm.get('quest_detail_image').setValue(this.quest_detail_image.name);
        var reader = new FileReader();
        reader.readAsDataURL(this.quest_detail_image); 
        reader.onload = (_event) => { 
          this.preview_detail_image = reader.result; 
        }
    }

    DtlImage1Change(files: FileList) {
        this.quest_detail_image1 = files[0];
        this.questForm.get('quest_detail_image1').setValue(this.quest_detail_image1.name);
        var reader = new FileReader();
        reader.readAsDataURL(this.quest_detail_image1); 
        reader.onload = (_event) => { 
          this.preview_detail_image1 = reader.result; 
        }
    }

    DtlImage2Change(files: FileList) {
        this.quest_detail_image2 = files[0];
        this.questForm.get('quest_detail_image2').setValue(this.quest_detail_image2.name);
        var reader = new FileReader();
        reader.readAsDataURL(this.quest_detail_image2); 
        reader.onload = (_event) => { 
          this.preview_detail_image2 = reader.result; 
        }
    }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.questForm.invalid) {
            return;
        }

        this.loading = true;

        const formData = new FormData();
        formData.append('quest_name', this.questForm.get('quest_name').value);
        formData.append('quest_short_description', this.questForm.get('quest_short_description').value);
        // formData.append('quest_description', this.questForm.get('quest_description').value);
        // formData.append('quest_expiry_date', (this.questForm.get('quest_expiry_date').value).toISOString());
        formData.append('quest_voucher_id', this.questForm.get('quest_voucher_id').value);

        formData.append('quest_status', this.questForm.get('quest_status').value);
        formData.append('quest_publish_date', (new Date(`${this.questForm.get('quest_publish_date').value} UTC`)).toISOString());

        formData.append('quest_limit_option', this.questForm.get('quest_limit_option').value);
        formData.append('quest_limit', this.questForm.get('quest_limit').value);

        formData.append('quest_expiry_option', this.questForm.get('quest_expiry_option').value);
        formData.append('quest_expiry_days', this.questForm.get('quest_expiry_days').value);

        if(this.questForm.get('quest_expiry_option').value=='1') {
          formData.append('quest_expiry_date', (new Date(`${this.questForm.get('quest_expiry_date').value} UTC`)).toISOString());
          
          if(this.questForm.get('quest_expiry_start_date').value) {
            formData.append('quest_expiry_start_date', (new Date(`${this.questForm.get('quest_expiry_start_date').value} UTC`)).toISOString());
          }

        }

        if(this.quest_challenge_icon!=null) {
          formData.append('quest_challenge_icon', this.quest_challenge_icon, this.quest_challenge_icon.name);
        }

        if(this.quest_challenge_image!=null) {
          formData.append('quest_challenge_image', this.quest_challenge_image, this.quest_challenge_image.name);
        }

        if(this.quest_detail_image!=null) {
          formData.append('quest_detail_image', this.quest_detail_image, this.quest_detail_image.name);
        }

        if(this.quest_detail_image1!=null) {
          formData.append('quest_detail_image1', this.quest_detail_image1, this.quest_detail_image1.name);
        }

        if(this.quest_detail_image2!=null) {
          formData.append('quest_detail_image2', this.quest_detail_image2, this.quest_detail_image2.name);
        }

        formData.append('quest_detail', this.questForm.get('quest_detail').value);
        formData.append('quest_detail1', this.questForm.get('quest_detail1').value);
        formData.append('quest_detail2', this.questForm.get('quest_detail2').value);
        
        // formData.append('quest_sort_order', this.questForm.get('quest_sort_order').value);

        this.questService.update(this.quest_id, formData).pipe(first()).subscribe((data: any) => {
              if(data.status=='ok') {
                this.alertService.success(data.message, true);
                this.router.navigate(['/quest']);
              } else {
                this.alertService.error(data.message, true);
              }
            },
            error => {
                this.alertService.error(error);
                this.loading = false;
            }
        );
    }
}