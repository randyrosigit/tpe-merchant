import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
import {MatTableDataSource, MatSort, MatPaginator} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';

import { AlertService, QuestService } from '../../_services';

@Component({ templateUrl: 'questcomplete.requests.html' })
export class QuestcompleteRequests implements OnInit, OnDestroy {
    
    heading = 'Quest Complete Requests';
	subheading = ''; 
	icon = 'fa fa-cubes icon-gradient bg-happy-itmeo';
    quests: [];
    dataSource: MatTableDataSource<any[]>;
    search_key = "";
    pageSizeOptions = null;
    filter_status = "";

    displayedColumns = [ 's_no','quest_name', 'customer_name', 'customer_email', 'quest_accepted_status', 'actions'];
    

    @ViewChild(MatPaginator) paginator: MatPaginator;  
    @ViewChild(MatSort) sort: MatSort;
    
    setPageSizeOptions(setPageSizeOptionsInput: string) {
      this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
    }

    constructor( 
        private questServices: QuestService,
        private alertService: AlertService 
    ) { }

    ngOnInit() {
        this.loadAllQuestRequests();
    }

    ngOnDestroy() {
    }

    clearFilters(){
        this.filter_status = "";
       this.dataSource.filter = '';
       this.search_key = '';
       this.loadAllQuestRequests();
    }

    private loadAllQuestRequests() {
        this.questServices.getAllCompleteRequests().pipe(first()).subscribe((quests: any) => {
        	if(quests.status=='ok') {
                this.dataSource = new MatTableDataSource(quests.result_set);
                this.dataSource.paginator = this.paginator;  
                this.dataSource.sort = this.sort; 
        	}
        });
    }
    
    acceptQuest(quest_id: string, customer_id: string) {

        const formData = new FormData();
        formData.append('quest_id', quest_id);
        formData.append('customer_id', customer_id);

        this.questServices.acceptQuest(formData).pipe(first()).subscribe((info: any) => {
            if(info.status=='ok') {
              this.alertService.showCode(info.message +': '+ info.result_set.quest_accepted_code);
            } else {
              this.alertService.error(info.message, true);
            }
        });
    }

    DeleteRequest(quest_id: string, customer_id: string, status: string) {

        const formData = new FormData();
        formData.append('quest_id', quest_id);
        formData.append('customer_id', customer_id);
        formData.append('status', status);

        this.questServices.deleteRequest(formData).pipe(first()).subscribe((info: any) => {
            if(info.status=='ok') {
              this.alertService.success(info.message);
              this.loadAllQuestRequests();
            } else {
              this.alertService.error(info.message, true);
            }
        });
    }


    applyFilter(filterValue: string) {  
      this.dataSource.filter = filterValue.trim().toLowerCase();  
    
      if (this.dataSource.paginator) {  
        this.dataSource.paginator.firstPage();  
      }  
    }

    private Filter() {
        this.questServices.getAllCompleteRequests({ params: { filter_status: this.filter_status}}).pipe(first()).subscribe((quests: any) => {
            if(quests.status=='ok') {
                this.dataSource = new MatTableDataSource(quests.result_set);
                this.dataSource.paginator = this.paginator;  
                this.dataSource.sort = this.sort; 
            }
        });
    }

}
