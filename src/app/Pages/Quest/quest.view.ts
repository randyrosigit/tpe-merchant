import { Component, OnInit, NgModule } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AlertService, QuestService } from '../../_services';
import { Utils } from '../../_helpers';

@Component({templateUrl: 'quest.view.html'})
export class QuestView implements OnInit {
    heading = 'View Quest';
    subheading = '';
    icon = 'fa fa-cubes icon-gradient bg-happy-itmeo';
    questForm: FormGroup;
    quest_id: number;
    preview_challenge_icon = null;
    preview_challenge_image = null;
    preview_detail_image = null;
    preview_detail_image1 = null;
    preview_detail_image2 = null;
    DateShow = null;

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private route: ActivatedRoute,
        private questService: QuestService,
        private alertService: AlertService,
    ) {  
        this.quest_id = this.route.snapshot.params['id'];
    }

    ngOnInit() {
        this.questForm = this.formBuilder.group({
            quest_name: ['', [Validators.required, Utils.noWhitespaceValidator]],
            // quest_description: ['', [Validators.required, Utils.noWhitespaceValidator]],
            quest_short_description: ['', [Validators.required, Utils.noWhitespaceValidator]],
            quest_voucher_name: ['', Validators.required],
            // quest_sort_order: [''],
            quest_limit_option: ['', Validators.required],
            quest_limit: ['', Validators.required],
            quest_expiry_option: ['', Validators.required],
            quest_expiry_start_date: [''],
            quest_expiry_date: [''],
            quest_expiry_days: [''],
            quest_status: ['', [Validators.required, Utils.noWhitespaceValidator]],
            quest_publish_date: ['', Validators.required],
        });
        this.getQuest(this.quest_id);
    }

    // convenience getter for easy access to form fields
    get f() { return this.questForm.controls; }

    getQuest(id) {
        this.questService.getById(id).subscribe((data: any) => {
            var quest = data.result_set;

            if(quest.quest_expiry_option=='1') {
                this.DateShow = true;
            } else {
                this.DateShow = false;
            }

            this.questForm.setValue({
              quest_name: quest.quest_name,
              // quest_description: quest.quest_description1,
              quest_short_description: quest.quest_short_description,
              quest_expiry_date: quest.quest_expiry_date1,
              quest_voucher_name: quest.voucher_name,
              // quest_sort_order: quest.quest_sort_order,
              quest_limit_option: quest.quest_limit_option,
              quest_limit: quest.quest_limit,
              quest_expiry_option: quest.quest_expiry_option,
              quest_expiry_start_date: quest.quest_expiry_start_date1,
              quest_expiry_days: quest.quest_expiry_days,
              quest_status: quest.quest_status,
              quest_publish_date: quest.quest_publish_date1,
            });
            this.preview_challenge_icon = quest.quest_challenge_icon;
            this.preview_challenge_image = quest.quest_challenge_image;
            this.preview_detail_image = quest.quest_detail_image;
            this.preview_detail_image1 = quest.quest_detail_image1;
            this.preview_detail_image2 = quest.quest_detail_image2;
        });
    }

    getControlLabel(type: string){
     return this.questForm.controls[type].value;
    }

}