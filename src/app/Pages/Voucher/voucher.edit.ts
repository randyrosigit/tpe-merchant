import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import {AngularEditorConfig} from '@kolkov/angular-editor';

import { AlertService, VoucherService } from '../../_services';
import { Utils } from '../../_helpers';

@Component({templateUrl: 'voucher.edit.html'})
export class VoucherEdit implements OnInit {
    heading = 'Edit Voucher';
    subheading = '';
    icon = 'fa fa-ticket icon-gradient bg-happy-itmeo';
    voucherForm: FormGroup;
    loading = false;
    submitted = false;
    voucher_challenge_icon = null;
    voucher_redeem_icon = null;
    minDate = new Date();
    currentDate = new Date();
    preview_challenge_icon = null;
    preview_redeem_icon = null;
    voucher_id: number;
    ExpminDate = null;
    ExpmaxDate = null;
    DateShow = null;
    StockType = false;

    wyswigconfig: AngularEditorConfig = {
      editable: true,
      spellcheck: true,
      height: '15rem',
      minHeight: '5rem',
      placeholder: 'Enter text here...',
      translate: 'no',
      customClasses: [
        {
          name: 'quote',
          class: 'quote',
        },
        {
          name: 'redText',
          class: 'redText'
        },
        {
          name: 'titleText',
          class: 'titleText',
          tag: 'h1',
        },
      ]
    };

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private route: ActivatedRoute,
        private alertService: AlertService,
        private voucherServices: VoucherService
    ) {  
        this.voucher_id = this.route.snapshot.params['id'];
    }

    ngOnInit() {
        this.voucherForm = this.formBuilder.group({
            voucher_type: ['', Validators.required],
            voucher_name: ['', [Validators.required, Utils.noWhitespaceValidator]],
            // voucher_short_description: ['', [Validators.required, Utils.noWhitespaceValidator]],
            voucher_description: ['', [Validators.required, Utils.noWhitespaceValidator]],
            // voucher_expiry_date: ['', Validators.required],
            voucher_expiry_option: ['', Validators.required],
            voucher_expiry_start_date: [''],
            voucher_expiry_date: [''],
            voucher_expiry_days: [''],
            voucher_buy_amount: ['', [Validators.required, Validators.min(1)]],
            voucher_value: ['', [Validators.required, Validators.min(1)]],
            voucher_stock_type: ['', Validators.required],
            voucher_stock: [''],
            // voucher_redeem_limit: ['', [Validators.required, Validators.min(1), Utils.noWhitespaceValidator]],
            voucher_challenge_icon: ['', [Validators.required, Utils.noWhitespaceValidator]],
            voucher_redeem_icon: ['', [Validators.required, Utils.noWhitespaceValidator]],
            voucher_status: ['', [Validators.required, Utils.noWhitespaceValidator]],
            voucher_publish_date: ['', Validators.required],
            // voucher_sort_order: ['']
        });
        this.getVoucher(this.voucher_id);
        this.StatusChange();
        this.ExpOptionChange();
        this.ExpStrtDateChange();
        this.ExpEndDateChange();
        this.StockChange();
    }

    StatusChange() {
      this.voucherForm.get('voucher_status').valueChanges.subscribe((voucherStatus: any) => {
        if(voucherStatus=='1') {
          this.currentDate = new Date();
          if(!this.voucherForm.get('voucher_publish_date')) {
            this.voucherForm.get('voucher_publish_date').setValue(this.minDate);
          }
        } else {
          if(!this.voucherForm.get('voucher_publish_date')) {
            this.voucherForm.get('voucher_publish_date').setValue('');
          }
          this.currentDate = null;           
        }
      });
    }

    ExpStrtDateChange() {
      this.voucherForm.get('voucher_expiry_start_date').valueChanges.subscribe((Date: any) => {
        if(Date) {
          this.ExpminDate = Date;
        }
      });
    }

    ExpEndDateChange() {
      this.voucherForm.get('voucher_expiry_date').valueChanges.subscribe((Date: any) => {
        if(Date) {
          this.ExpmaxDate = Date;
        }
      });
    }

    ExpOptionChange() {
      this.voucherForm.get('voucher_expiry_option').valueChanges.subscribe((option: any) => {
        if(option=='1') {
          this.voucherForm.get('voucher_expiry_days').setValidators(null);
          this.voucherForm.get('voucher_expiry_date').setValidators([Validators.required]);
          this.DateShow = true;
        } else {
          this.voucherForm.get('voucher_expiry_date').setValidators(null);
          this.voucherForm.get('voucher_expiry_days').setValidators([Validators.required, Validators.min(1)]);
          this.DateShow = false;
        }
        this.voucherForm.get('voucher_expiry_days').updateValueAndValidity();
        this.voucherForm.get('voucher_expiry_date').updateValueAndValidity();
      });
    }

    StockChange() {
      this.voucherForm.get('voucher_stock_type').valueChanges.subscribe((option: any) => {
        if(option=='1') {
          this.StockType = true;
          this.voucherForm.get('voucher_stock').setValidators([Validators.required, Validators.min(1)]);
        } else {
          this.StockType = false;
          this.voucherForm.get('voucher_stock').setValidators(null);
          this.voucherForm.get('voucher_stock').setValue(null);
        }
        this.voucherForm.get('voucher_stock').updateValueAndValidity();
      });
    }

    // convenience getter for easy access to form fields
    get f() { return this.voucherForm.controls; }

    getVoucher(id) {
        this.voucherServices.getById(id).subscribe((data: any) => {
            var voucher = data.result_set;

            var voucher_expiry_start_date = null;
            var voucher_expiry_date = null;
            var voucher_expiry_days = null;

            if(voucher.voucher_expiry_option=='1') {
              if(voucher.voucher_expiry_start_date) {
                voucher_expiry_start_date = new Date(voucher.voucher_expiry_start_date);
              }
              if(voucher.voucher_expiry_end_date) {
                voucher_expiry_date = new Date(voucher.voucher_expiry_end_date);
              }
            } else {
              voucher_expiry_days = voucher.voucher_expiry_days;
            }

            this.voucherForm.setValue({
              voucher_type: voucher.voucher_type,
              voucher_name: voucher.voucher_name,
              // voucher_short_description: voucher.voucher_short_description,
              voucher_description: voucher.voucher_description,
              voucher_buy_amount: voucher.voucher_buy_amount,
              voucher_value: voucher.voucher_value,
              voucher_stock_type: voucher.voucher_stock_type,
              voucher_stock: voucher.voucher_stock,
              // voucher_redeem_limit: voucher.voucher_redeem_limit,
              voucher_expiry_option: voucher.voucher_expiry_option,
              voucher_expiry_start_date: voucher_expiry_start_date,
              voucher_expiry_date: voucher_expiry_date,
              voucher_expiry_days: voucher_expiry_days,
              voucher_challenge_icon: voucher.voucher_challenge_icon,
              voucher_redeem_icon: voucher.voucher_redeem_icon,
              voucher_status: voucher.voucher_status,
              voucher_publish_date: new Date(voucher.voucher_publish_date),
              // voucher_sort_order: voucher.voucher_sort_order
            });
            this.preview_challenge_icon = voucher.voucher_challenge_icon;
            this.preview_redeem_icon = voucher.voucher_redeem_icon;
        });
    }

    IconChange(files: FileList) {
        this.voucher_challenge_icon = files[0];
        this.voucherForm.get('voucher_challenge_icon').setValue(this.voucher_challenge_icon.name);
        var reader = new FileReader();
        reader.readAsDataURL(this.voucher_challenge_icon); 
        reader.onload = (_event) => { 
          this.preview_challenge_icon = reader.result; 
        }
    }

    ImageChange(files: FileList) {
        this.voucher_redeem_icon = files[0];
        this.voucherForm.get('voucher_redeem_icon').setValue(this.voucher_redeem_icon.name);
        var reader = new FileReader();
        reader.readAsDataURL(this.voucher_redeem_icon); 
        reader.onload = (_event) => { 
          this.preview_redeem_icon = reader.result; 
        }
    }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.voucherForm.invalid) {
            return;
        }

        this.loading = true;

        const formData = new FormData();
        formData.append('voucher_type', this.voucherForm.get('voucher_type').value);
        formData.append('voucher_name', this.voucherForm.get('voucher_name').value);
        // formData.append('voucher_short_description', this.voucherForm.get('voucher_short_description').value);
        formData.append('voucher_description', this.voucherForm.get('voucher_description').value);
        formData.append('voucher_buy_amount', this.voucherForm.get('voucher_buy_amount').value);
        formData.append('voucher_value', this.voucherForm.get('voucher_value').value);
        formData.append('voucher_stock_type', this.voucherForm.get('voucher_stock_type').value);
        formData.append('voucher_stock', this.voucherForm.get('voucher_stock').value);
        // formData.append('voucher_redeem_limit', this.voucherForm.get('voucher_redeem_limit').value);
        // formData.append('voucher_expiry_date', (this.voucherForm.get('voucher_expiry_date').value).toISOString());
        // formData.append('voucher_sort_order', this.voucherForm.get('voucher_sort_order').value);
        formData.append('voucher_status', this.voucherForm.get('voucher_status').value);
        formData.append('voucher_publish_date', (new Date(`${this.voucherForm.get('voucher_publish_date').value} UTC`)).toISOString());

        formData.append('voucher_expiry_option', this.voucherForm.get('voucher_expiry_option').value);

        formData.append('voucher_expiry_days', this.voucherForm.get('voucher_expiry_days').value);

        if(this.voucherForm.get('voucher_expiry_option').value=='1') {
          formData.append('voucher_expiry_date', (new Date(`${this.voucherForm.get('voucher_expiry_date').value} UTC`)).toISOString());
          
          if(this.voucherForm.get('voucher_expiry_start_date').value) {
            formData.append('voucher_expiry_start_date', (new Date(`${this.voucherForm.get('voucher_expiry_start_date').value} UTC`)).toISOString());
          }

        }

        if(this.voucher_challenge_icon!=null) {
          formData.append('voucher_challenge_icon', this.voucher_challenge_icon, this.voucher_challenge_icon.name);
        }

        if(this.voucher_redeem_icon!=null) {
          formData.append('voucher_redeem_icon', this.voucher_redeem_icon, this.voucher_redeem_icon.name);
        }

        this.voucherServices.update(this.voucher_id, formData).pipe(first()).subscribe((data: any) => {
              if(data.status=='ok') {
                this.alertService.success(data.message, true);
                this.router.navigate(['/voucher']);
              } else {
                this.alertService.error(data.message, true);
              }
            },
            error => {
                this.alertService.error(error);
                this.loading = false;
            }
        );
    }
}