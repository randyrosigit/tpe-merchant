import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import {AngularEditorConfig} from '@kolkov/angular-editor';

import { AlertService, VoucherService } from '../../_services';
import { Utils } from '../../_helpers';

@Component({ templateUrl: 'voucher.add.html'})
export class VoucherAdd implements OnInit {
    heading = 'Add Voucher';
    subheading = '';
    icon = 'fa fa-ticket icon-gradient bg-happy-itmeo';
    voucherForm: FormGroup;
    loading = false;
    submitted = false;
    voucher_challenge_icon = null;
    voucher_redeem_icon = null;
    minDate = new Date();
    ExpminDate = new Date();
    ExpmaxDate = null;
    currentDate = new Date();
    preview_challenge_icon = null;
    preview_redeem_icon = null;
    DateShow = true;
    StockType = false;

    wyswigconfig: AngularEditorConfig = {
      editable: true,
      spellcheck: true,
      height: '15rem',
      minHeight: '5rem',
      placeholder: 'Enter text here...',
      translate: 'no',
      customClasses: [
        {
          name: 'quote',
          class: 'quote',
        },
        {
          name: 'redText',
          class: 'redText'
        },
        {
          name: 'titleText',
          class: 'titleText',
          tag: 'h1',
        },
      ]
    };

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private alertService: AlertService,
        private voucherServices: VoucherService
    ) { }

    ngOnInit() {
        this.voucherForm = this.formBuilder.group({
            voucher_type: ['0', Validators.required],
            voucher_name: ['', [Validators.required, Utils.noWhitespaceValidator]],
            // voucher_short_description: ['', [Validators.required, Utils.noWhitespaceValidator]],
            voucher_description: ['', Validators.required],
            voucher_expiry_option: ['1', Validators.required],
            voucher_expiry_start_date: [''],
            voucher_expiry_date: ['', Validators.required],
            voucher_expiry_days: [''],
            voucher_buy_amount: ['', [Validators.required, Validators.min(1)]],
            voucher_value: ['', [Validators.required, Validators.min(1)]],
            voucher_stock: [''],
            voucher_stock_type: ['0', Validators.required],
            // voucher_redeem_limit: ['', [Validators.required, Validators.min(1)]],
            voucher_challenge_icon: ['', Validators.required],
            voucher_redeem_icon: ['', Validators.required],
            voucher_status: ['', Validators.required],
            voucher_publish_date: ['', Validators.required],
            // voucher_sort_order: ['']
        });
        this.StatusChange();
        this.ExpOptionChange();
        this.ExpStrtDateChange();
        this.ExpEndDateChange();
        this.StockChange();
    }
    
    StatusChange() {
      this.voucherForm.get('voucher_status').valueChanges.subscribe((voucherStatus: any) => {
        if(voucherStatus=='0') {
          this.voucherForm.get('voucher_publish_date').setValue('');
          this.currentDate = null;
        } else {
          this.currentDate = new Date();
          this.voucherForm.get('voucher_publish_date').setValue(this.minDate); 
        }
      });
    }

    ExpStrtDateChange() {
      this.voucherForm.get('voucher_expiry_start_date').valueChanges.subscribe((Date: any) => {
        if(Date) {
          this.ExpminDate = Date;
        }
      });
    }

    ExpEndDateChange() {
      this.voucherForm.get('voucher_expiry_date').valueChanges.subscribe((Date: any) => {
        if(Date) {
          this.ExpmaxDate = Date;
        }
      });
    }

    ExpOptionChange() {
      this.voucherForm.get('voucher_expiry_option').valueChanges.subscribe((option: any) => {
        if(option=='1') {
          this.voucherForm.get('voucher_expiry_days').setValidators(null);
          this.voucherForm.get('voucher_expiry_date').setValidators([Validators.required]);
          this.DateShow = true;
        } else {
          this.voucherForm.get('voucher_expiry_date').setValidators(null);
          this.voucherForm.get('voucher_expiry_days').setValidators([Validators.required, Validators.min(1)]);
          this.DateShow = false;
        }
        this.voucherForm.get('voucher_expiry_days').updateValueAndValidity();
        this.voucherForm.get('voucher_expiry_date').updateValueAndValidity();
      });
    }

    IconChange(files: FileList) {
        this.voucher_challenge_icon = files[0];
        this.voucherForm.get('voucher_challenge_icon').setValue(this.voucher_challenge_icon.name);
        var reader = new FileReader();
        reader.readAsDataURL(this.voucher_challenge_icon); 
        reader.onload = (_event) => { 
          this.preview_challenge_icon = reader.result; 
        }
    }

    ImageChange(files: FileList) {
        this.voucher_redeem_icon = files[0];
        this.voucherForm.get('voucher_redeem_icon').setValue(this.voucher_redeem_icon.name);
        var reader = new FileReader();
        reader.readAsDataURL(this.voucher_redeem_icon); 
        reader.onload = (_event) => { 
          this.preview_redeem_icon = reader.result; 
        }
    }

    StockChange() {
      this.voucherForm.get('voucher_stock_type').valueChanges.subscribe((option: any) => {
        if(option=='1') {
          this.StockType = true;
          this.voucherForm.get('voucher_stock').setValidators([Validators.required, Validators.min(1)]);
        } else {
          this.StockType = false;
          this.voucherForm.get('voucher_stock').setValidators(null);
          this.voucherForm.get('voucher_stock').setValue(null);
        }
        this.voucherForm.get('voucher_stock').updateValueAndValidity();
      });
    }

    // convenience getter for easy access to form fields
    get f() { return this.voucherForm.controls; }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.voucherForm.invalid) {
            return;
        }

        this.loading = true;

        const formData = new FormData();
        formData.append('voucher_type', this.voucherForm.get('voucher_type').value);
        formData.append('voucher_name', this.voucherForm.get('voucher_name').value);
        // formData.append('voucher_short_description', this.voucherForm.get('voucher_short_description').value);
        formData.append('voucher_description', this.voucherForm.get('voucher_description').value);
        formData.append('voucher_buy_amount', this.voucherForm.get('voucher_buy_amount').value);
        formData.append('voucher_value', this.voucherForm.get('voucher_value').value);
        formData.append('voucher_stock_type', this.voucherForm.get('voucher_stock_type').value);
        formData.append('voucher_stock', this.voucherForm.get('voucher_stock').value);
        // formData.append('voucher_redeem_limit', this.voucherForm.get('voucher_redeem_limit').value);
        // formData.append('voucher_expiry_date', (this.voucherForm.get('voucher_expiry_date').value).toISOString());
        formData.append('voucher_challenge_icon', this.voucher_challenge_icon, this.voucher_challenge_icon.name);
        formData.append('voucher_redeem_icon', this.voucher_redeem_icon, this.voucher_redeem_icon.name);
        // formData.append('voucher_sort_order', this.voucherForm.get('voucher_sort_order').value);
        formData.append('voucher_status', this.voucherForm.get('voucher_status').value);
        formData.append('voucher_publish_date', (new Date(`${this.voucherForm.get('voucher_publish_date').value} UTC`)).toISOString());
        formData.append('voucher_expiry_option', this.voucherForm.get('voucher_expiry_option').value);

        formData.append('voucher_expiry_days', this.voucherForm.get('voucher_expiry_days').value);

        if(this.voucherForm.get('voucher_expiry_option').value=='1') {
          formData.append('voucher_expiry_date', (new Date(`${this.voucherForm.get('voucher_expiry_date').value} UTC`)).toISOString());
          
          if(this.voucherForm.get('voucher_expiry_start_date').value) {
            formData.append('voucher_expiry_start_date', (new Date(`${this.voucherForm.get('voucher_expiry_start_date').value} UTC`)).toISOString());
          }

        }

        this.voucherServices.insert(formData).pipe(first()).subscribe((data: any) => {
            if(data.status=='ok') {
              this.alertService.success(data.message, true);
              this.router.navigate(['/voucher']);
            } else {
              this.alertService.error(data.message, true);
            }
          },
          error => {
              this.alertService.error(error);
              this.loading = false;
          }
        );
    }
}