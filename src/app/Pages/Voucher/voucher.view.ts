import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AlertService, VoucherService } from '../../_services';
import { Utils } from '../../_helpers';

@Component({templateUrl: 'voucher.view.html'})
export class VoucherView implements OnInit {
    heading = 'View Voucher';
    subheading = '';
    icon = 'fa fa-ticket icon-gradient bg-happy-itmeo';
    voucherForm: FormGroup;
    preview_challenge_icon = null;
    preview_redeem_icon = null;
    voucher_id: number;
    DateShow = null;
    stockStatus = false;

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private route: ActivatedRoute,
        private alertService: AlertService,
        private voucherServices: VoucherService
    ) {  
        this.voucher_id = this.route.snapshot.params['id'];
    }

    ngOnInit() {
        this.voucherForm = this.formBuilder.group({
            voucher_type: ['', Validators.required],
            voucher_name: ['', [Validators.required, Utils.noWhitespaceValidator]],
            voucher_description: ['', [Validators.required, Utils.noWhitespaceValidator]],
            // voucher_expiry_date: ['', Validators.required],
            voucher_expiry_option: ['', Validators.required],
            voucher_expiry_start_date: [''],
            voucher_expiry_date: [''],
            voucher_expiry_days: [''],
            voucher_buy_amount: ['', Validators.required],
            voucher_value: ['', Validators.required],
            voucher_stock_type: ['', Validators.required],
            voucher_stock: ['', Validators.required],
            // voucher_redeem_limit: ['', [Validators.required, Utils.noWhitespaceValidator]],
            voucher_status: ['', [Validators.required, Utils.noWhitespaceValidator]],
            voucher_publish_date: ['', Validators.required],
            // voucher_sort_order: ['']
        });
        this.getVoucher(this.voucher_id);
    }

    // convenience getter for easy access to form fields
    get f() { return this.voucherForm.controls; }

    getVoucher(id) {
        this.voucherServices.getById(id).subscribe((data: any) => {
            var voucher = data.result_set;

            if(voucher.voucher_expiry_option=='1') {
                this.DateShow = true;
            } else {
                this.DateShow = false;
            }

            if(voucher.voucher_stock_type=='1') {
              this.stockStatus = true;
            }

            this.voucherForm.setValue({
              voucher_type: voucher.voucher_type,
              voucher_name: voucher.voucher_name,
              voucher_description: voucher.voucher_description1,
              voucher_expiry_option: voucher.voucher_expiry_option,
              voucher_expiry_start_date: voucher.voucher_expiry_start_date1,
              voucher_expiry_date: voucher.voucher_expiry_date1,
              voucher_expiry_days: voucher.voucher_expiry_days,
              voucher_buy_amount: voucher.voucher_buy_amount,
              voucher_value: voucher.voucher_value,
              voucher_stock: voucher.voucher_stock,
              voucher_stock_type: voucher.voucher_stock_type,
              // voucher_redeem_limit: voucher.voucher_redeem_limit,
              // voucher_sort_order: voucher.voucher_sort_order,
              voucher_status: voucher.voucher_status,
              voucher_publish_date: voucher.voucher_publish_date1,
            });
            this.preview_challenge_icon = voucher.voucher_challenge_icon;
            this.preview_redeem_icon = voucher.voucher_redeem_icon;
        });
    }

    getControlLabel(type: string){
     return this.voucherForm.controls[type].value;
    }
}