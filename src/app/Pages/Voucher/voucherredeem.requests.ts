import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
import {MatTableDataSource, MatSort, MatPaginator} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import Swal from 'sweetalert2';

import { AlertService, VoucherService } from '../../_services';

@Component({ templateUrl: 'voucherredeem.requests.html' })
export class VoucherredeemRequests implements OnInit, OnDestroy {
    
    heading = 'Voucher Redeem Requests';
	subheading = ''; 
	icon = 'fa fa-ticket icon-gradient bg-happy-itmeo';
    quests: [];
    dataSource: MatTableDataSource<any[]>;
    search_key = "";
    pageSizeOptions = null;
    filter_status = "";
    vouchers: any;

    displayedColumns = [ 's_no','voucher_name', 'customer_name', 'customer_email', 'voucher_status', 'actions'];
    

    @ViewChild(MatPaginator) paginator: MatPaginator;  
    @ViewChild(MatSort) sort: MatSort;
    
    setPageSizeOptions(setPageSizeOptionsInput: string) {
      this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
    }

    constructor( 
        private voucherServices: VoucherService,
        private alertService: AlertService 
    ) { }

    ngOnInit() {
        this.loadAllRedeemRequests();
        this.loadAllVouchers();
    }

    ngOnDestroy() {
    }

    clearFilters(){
      this.filter_status = "";
       this.dataSource.filter = '';
       this.search_key = '';
       this.loadAllRedeemRequests();
    }

    private loadAllVouchers() {
        this.voucherServices.getAll({ params: {filter_valid_voucher:'yes'} }).pipe(first()).subscribe((vouchers: any) => {
            if(vouchers.status=='ok') {
              let voucherss: any = {};
              vouchers.result_set.forEach(voucher=> {
                voucherss[voucher.voucher_id] = voucher.voucher_name;
              })
              this.vouchers = voucherss;
            }
        });
    }

    private loadAllRedeemRequests() {
      this.voucherServices.getAllRedeemRequests().pipe(first()).subscribe((vouchers: any) => {
      	if(vouchers.status=='ok') {
              this.dataSource = new MatTableDataSource(vouchers.result_set);
              this.dataSource.paginator = this.paginator;  
              this.dataSource.sort = this.sort; 
      	}
      });
    }
    
    RedeemComplete(my_voucher_id: string, customer_id: string) {

        const currentObj = this;

        Swal({
          title: 'Enter Redeem Code',
          input: 'text',
          inputAttributes: {
            autocapitalize: 'off',
            required: 'true'
          },
          showCancelButton: true,
          confirmButtonText: 'Submit',
          showLoaderOnConfirm: true,
          preConfirm: (code) => {

            if(code=='') {
                Swal.showValidationMessage(
                  `Request failed:Enter code to continue`
                )
                return;
            }

            const formData = new FormData();
            formData.append('my_voucher_id', my_voucher_id);
            formData.append('customer_id', customer_id);
            formData.append('code', code);

            return currentObj.voucherServices.CompleteRedeem(formData).pipe(first()).subscribe((info: any) => {
                if(info.status=='ok') {
                  currentObj.alertService.success(info.message);
                  currentObj.loadAllRedeemRequests();
                } else {
                  this.alertService.error(info.message, true);
                }
            });

          },
          allowOutsideClick: () => !Swal.isLoading()
        });
    }

    VoucherChange(my_voucher_id: string, customer_id: string) {

        const currentObj = this;

        Swal({
          title: 'Voucher Exchange',
          input: 'select',
          inputAttributes: {
            required: 'true'
          },
          inputOptions: currentObj.vouchers,
          inputPlaceholder: 'Select a voucher',
          showCancelButton: true,
          confirmButtonText: 'Submit',
          showLoaderOnConfirm: true,
          preConfirm: (code) => {

            if(code=='') {
                Swal.showValidationMessage(
                  `Request failed:Enter code to continue`
                )
                return;
            }

            const formData = new FormData();
            formData.append('my_voucher_id', my_voucher_id);
            formData.append('customer_id', customer_id);
            formData.append('new_voucher_id', code);

            return currentObj.voucherServices.VoucherExchange(formData).pipe(first()).subscribe((info: any) => {
                if(info.status=='ok') {
                  currentObj.alertService.success(info.message);
                  currentObj.loadAllRedeemRequests();
                } else {
                  this.alertService.error(info.message, true);
                }
            }, error => {
                this.alertService.error(error);
            });

          },
          allowOutsideClick: () => !Swal.isLoading()
        });
    }

    applyFilter(filterValue: string) {  
      this.dataSource.filter = filterValue.trim().toLowerCase();  
    
      if (this.dataSource.paginator) {  
        this.dataSource.paginator.firstPage();  
      }  
    }

    DeleteRequest(my_voucher_id, status) {

      const formData = new FormData();
      formData.append('my_voucher_id', my_voucher_id);
      formData.append('status', status);

      this.voucherServices.DeleteRequest(formData).pipe(first()).subscribe((data: any) => {
          if(data.status=='ok') {
            this.alertService.success(data.message, true);
            this.loadAllRedeemRequests();
          } else {
            this.alertService.error(data.message, true);
          }
      }, error => {
          this.alertService.error(error);
      });
    }

    private Filter() {
      this.voucherServices.getAllRedeemRequests({ params: { filter_status: this.filter_status}}).pipe(first()).subscribe((vouchers: any) => {
        if(vouchers.status=='ok') {
              this.dataSource = new MatTableDataSource(vouchers.result_set);
              this.dataSource.paginator = this.paginator;  
              this.dataSource.sort = this.sort; 
        }
      });
    }

}
