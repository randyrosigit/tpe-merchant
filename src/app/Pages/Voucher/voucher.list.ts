import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
import {MatTableDataSource, MatSort, MatPaginator} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';

// import { Voucher } from '../../_models';
import { AlertService, VoucherService } from '../../_services';

@Component({ templateUrl: 'voucher.list.html' })
export class VoucherList implements OnInit, OnDestroy {
    
    heading = 'Vouchers';
	  subheading = '';
	  icon = 'fa fa-ticket icon-gradient bg-happy-itmeo';
	  link = '/voucher/add';
    vouchers: [];
    dataSource: MatTableDataSource<any[]>;
    search_key = "";
    pageSizeOptions = null;

    displayedColumns = [ 's_no','voucher_name', 'voucher_type', 'voucher_buy_amount', 'voucher_stock', 'voucher_admin_approved', 'voucher_created_on', 'actions'];
    

    @ViewChild(MatPaginator) paginator: MatPaginator;  
    @ViewChild(MatSort) sort: MatSort;
    
    setPageSizeOptions(setPageSizeOptionsInput: string) {
      this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
    }

    constructor( 
        private voucherServices: VoucherService,
        private alertService: AlertService ) { }

    ngOnInit() {
        this.loadAllVouchers();
    }

    ngOnDestroy() {
    }

    clearFilters(){
       this.dataSource.filter = '';
       this.search_key = '';
    }

    private loadAllVouchers() {
        this.voucherServices.getAll().pipe(first()).subscribe((vouchers: any) => {
        	if(vouchers.status=='ok') {
                this.dataSource = new MatTableDataSource(vouchers.result_set);
                this.dataSource.paginator = this.paginator;  
                this.dataSource.sort = this.sort; 
        	}
        });
    }

    deleteVoucher(id: number) {
        this.alertService.delete().then(data=>{
            if(data) {
                this.voucherServices.delete(id).pipe(first()).subscribe((data: any) => {
                    if(data.status=='ok') {
                      this.alertService.success(data.message, true);
                      this.loadAllVouchers()
                    } else {
                      this.alertService.error(data.message, true);
                    }
                });
            } 
        });
    }

    featuredVoucher(id: number) {
        this.voucherServices.featured(id).pipe(first()).subscribe((data: any) => {
            if(data.status=='ok') {
              this.alertService.success(data.message, true);
              this.loadAllVouchers()
            } else {
              this.alertService.error(data.message, true);
            }
        });
    }

    approvedVoucher(id: number) {
        this.voucherServices.approved(id).pipe(first()).subscribe((data: any) => {
            if(data.status=='ok') {
              this.alertService.success(data.message, true);
              this.loadAllVouchers()
            } else {
              this.alertService.error(data.message, true);
            }
        });
    }

    applyFilter(filterValue: string) {  
      this.dataSource.filter = filterValue.trim().toLowerCase();  
    
      if (this.dataSource.paginator) {  
        this.dataSource.paginator.firstPage();  
      }  
    }

}
